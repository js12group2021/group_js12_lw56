import { Component } from '@angular/core';
import {Ingredient} from "./shared/model.ingredient";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  ingredients: Ingredient[] = [];

  onNewIng(ingr:Ingredient){
    this.ingredients.push(ingr);
  }
}
