import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ChesComponent } from './ches.component';

describe('ChesComponent', () => {
  let component: ChesComponent;
  let fixture: ComponentFixture<ChesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ChesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ChesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
