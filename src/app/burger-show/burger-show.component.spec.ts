import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BurgerShowComponent } from './burger-show.component';

describe('BurgerShowComponent', () => {
  let component: BurgerShowComponent;
  let fixture: ComponentFixture<BurgerShowComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BurgerShowComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BurgerShowComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
