import {Component, Input} from '@angular/core';
import {Ingredient} from "../shared/model.ingredient";

@Component({
  selector: 'app-burger-show',
  templateUrl: './burger-show.component.html',
  styleUrls: ['./burger-show.component.css']
})
export class BurgerShowComponent {

 @Input() ingred!: Ingredient;

}
