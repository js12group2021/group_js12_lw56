import {Component, ElementRef, EventEmitter, OnInit, Output, ViewChild} from '@angular/core';
import {Ingredient} from "../shared/model.ingredient";

@Component({
  selector: 'app-salad',
  templateUrl: './salad.component.html',
  styleUrls: ['./salad.component.css']
})
export class SaladComponent {

  @Output() newIngred = new EventEmitter<Ingredient>();
  @ViewChild('linkIngr') linkIngr!: ElementRef;

  createIngredient(){
    console.log(this.linkIngr.nativeElement.value);
    const name = this.linkIngr.nativeElement.value;
    const ni = new Ingredient(name);
    this.newIngred.emit(ni);

  }

}
