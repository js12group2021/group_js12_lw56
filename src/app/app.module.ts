import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { IngredientsComponent } from './ingredients/ingredients.component';
import { BurgerShowComponent } from './burger-show/burger-show.component';
import { FormsModule } from "@angular/forms";
import { ChesComponent } from './ches/ches.component';
import { SaladComponent } from './salad/salad.component';
import { BaconComponent } from './bacon/bacon.component';



@NgModule({
  declarations: [
    AppComponent,
    IngredientsComponent,
    BurgerShowComponent,
    ChesComponent,
    SaladComponent,
    BaconComponent,

  ],
  imports: [
    BrowserModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
