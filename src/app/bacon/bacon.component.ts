import {Component, ElementRef, EventEmitter, OnInit, Output, ViewChild} from '@angular/core';
import {Ingredient} from "../shared/model.ingredient";

@Component({
  selector: 'app-bacon',
  templateUrl: './bacon.component.html',
  styleUrls: ['./bacon.component.css']
})
export class BaconComponent {

  @Output() newIngred = new EventEmitter<Ingredient>();
  @ViewChild('linkIngr') linkIngr!: ElementRef;

  createIngredient(){
    console.log(this.linkIngr.nativeElement.value);
    const name = this.linkIngr.nativeElement.value;
    const ni = new Ingredient(name);
    this.newIngred.emit(ni);

  }

}
